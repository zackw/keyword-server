import os
import random
from typing import Iterable, Optional, Set

PAUSES = ['，', '，', '，', '，', '、', '；', '：', ]
STOPS = ['。', '。', '。', '。', '。', '。', '。', '。', '。', '。',
         '！', '？', ]
CHARS_PER_SENTENCE = (5, 15)
SENTENCE_PER_PAR = (3, 10)
CHARS_PER_PARAGRAPH = (CHARS_PER_SENTENCE[0] * SENTENCE_PER_PAR[0],
                       CHARS_PER_SENTENCE[1] * SENTENCE_PER_PAR[1])

DATADIR = os.path.join(os.path.dirname(__file__), 'data')


def _load_words(fname: str, target_encoding: str) -> Set[str]:
    with open(fname, "rt", encoding="utf-8") as fp:
        rv = set()
        for line in fp:
            line = line.strip()
            if line:
                assert line.encode(target_encoding) != b""
                rv.add(line)
        return rv


WORDS_ZH_SIMP = _load_words(os.path.join(DATADIR, 'word-zh-simp.txt'),
                            "gb18030")
WORDS_ZH_TRAD = _load_words(os.path.join(DATADIR, 'word-zh-trad.txt'),
                            "big5-hkscs")

WORDS_ZH = WORDS_ZH_SIMP | WORDS_ZH_TRAD


class Lorem:
    def __init__(self, words: Iterable[str]) -> None:
        self.words = sorted(words)

    def word(self) -> str:
        return random.choice(self.words)

    def subsentence(self, length: int) -> str:
        words = []
        while length > 0:
            word = self.word()
            length -= len(word)
            words.append(word)
        return ''.join(words)

    def sentence(self, length: Optional[int] = None) -> str:
        length = length or random.randint(*CHARS_PER_SENTENCE)
        parts = []

        while length > 0:
            part = self.subsentence(min(
                length,
                random.randint(*CHARS_PER_SENTENCE)
            ))
            length -= (len(part) + 1)
            parts.append(part)
            parts.append(random.choice(PAUSES))

        parts[-1] = random.choice(STOPS)
        return ''.join(parts)

    def paragraph(self, length: Optional[int] = None) -> str:
        length = length or random.randint(*CHARS_PER_PARAGRAPH)
        sentences = []

        while length > 0:
            sentence = self.sentence(min(
                length,
                random.randint(*CHARS_PER_SENTENCE)
            ))
            length -= len(sentence)
            sentences.append(sentence)

        return ''.join(sentences)


LOREM_ZH_SIMP = Lorem(WORDS_ZH_SIMP)
LOREM_ZH_TRAD = Lorem(WORDS_ZH_TRAD)
LOREM_ZH = Lorem(WORDS_ZH)


def lorem(scripts: Set[str]) -> Lorem:
    wanted = ''.join(sorted(scripts))
    if wanted in ('s', 'as'):
        return LOREM_ZH_SIMP
    elif wanted in ('t', 'at'):
        return LOREM_ZH_TRAD
    else:  # ('a', 'st', 'ast')
        return LOREM_ZH
