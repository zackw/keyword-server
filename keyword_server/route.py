from os.path import dirname
from aiohttp.web import Application
from . import view


PROJECT_ROOT = dirname(__file__)


def setup(app: Application) -> None:
    app.router.add_get('/', view.index)
    app.router.add_get('/search', view.search)
    app.router.add_get('/whatisthat', view.search)
    app.router.add_get('/searchwhatisthat', view.search)
    app.router.add_get('/whatisthatsearch', view.search)
    app.router.add_get('/whatsearchisthat', view.search)
    app.router.add_get('/sitemap', view.sitemap)
    app.router.add_get('/update', view.good)
    app.router.add_get('/send', view.bad)
    app.router.add_get('/whatisthis', view.whatisthis)
    app.router.add_get('/whatisthis2', view.whatisthis)
    app.router.add_get('/hereitis', view.falundafa)
    setup_static(app)


def setup_static(app: Application) -> None:
    app.router.add_static('/static/',
                          path=PROJECT_ROOT + '/static',
                          name='static')
