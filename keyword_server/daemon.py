# Helper functions for interacting with the system service manager
# (currently only systemd is supported).

__all__ = ['notify_started', 'config_log', 'get_log_levels']

import functools
import logging
import logging.handlers
import os
import socket
import sys
from typing import Dict, NamedTuple, Optional, List, Sequence, cast


_log = logging.getLogger("keyword-server.daemon")


# Optionally, we can use pyroute2 to determine whether this system has
# *globally routable* IPv4 and IPv6 -- not just "at least one configured
# address that isn't the loopback" the way AI_ADDRCONFIG does.
try:
    import pyroute2  # type: ignore

    def which_families_are_routable() -> Sequence[int]:
        """Returns zero or more of AF_INET and AF_INET6, depending on
           which IP generations can be used to communicate with the
           world from this host.
        """
        generations = []
        with pyroute2.NDB() as ndb:
            for entry in ndb.routes.values():
                # Table 254 is the main routing table.
                # I have no idea where this is documented officially,
                # nor why that number was chosen.
                # Default routes have a destination of the empty string.
                if entry["table"] == 254 and entry["dst"] == "":
                    generations.append(entry["family"])
        return generations

except ImportError:
    _log.debug("pyroute2 not available")

    def which_families_are_routable() -> Sequence[int]:
        # We don't know.  Fall back to AI_ADDRCONFIG.
        return [socket.AF_UNSPEC]


# The systemd.daemon module may or may not be available.  If it isn't,
# we do not try to implement the notification protocol by hand.
try:
    from systemd.daemon import notify  # type: ignore
except ImportError:
    _log.debug("systemd.daemon.notify not available")

    def notify(status: str,
               unset_environment: bool = False,
               pid: int = 0,
               fds: Optional[Sequence[int]] = None) -> bool:
        # unset_environment is specified to work regardless of whether
        # any message was sent.
        if unset_environment:
            if "NOTIFY_SOCKET" in os.environ:
                del os.environ["NOTIFY_SOCKET"]
        return False  # no message actually sent


# We *do* reimplement the listen_fds interface by hand when necessary,
# because it's not that complicated (for the part we need, anyway)
try:
    from systemd.daemon import listen_fds  # type has already been ignored
except ImportError:
    _log.debug("systemd.daemon.listen_fds not available")

    def listen_fds(unset_environment: bool = True) -> Sequence[int]:
        try:
            if unset_environment:
                LISTEN_PID = os.environ.pop("LISTEN_PID")
                LISTEN_FDS = os.environ.pop("LISTEN_FDS")
                os.environ.pop("LISTEN_FDNAMES", None)
            else:
                LISTEN_PID = os.environ["LISTEN_PID"]
                LISTEN_FDS = os.environ["LISTEN_FDS"]

            if int(LISTEN_PID) != os.getpid():
                return []
            fds = []
            for fd in range(3, 3 + int(LISTEN_FDS)):
                try:
                    os.set_inheritable(fd, False)
                    fds.append(fd)
                except OSError:
                    # if we get here, `fd` wasn't actually open;
                    # do not add it to the list
                    pass

            return fds

        except Exception:
            # either the variables were not set, or we could not parse them
            return []


# Like the notification protocol, we do not try to implement the
# journal protocol by hand.
try:
    from systemd.journal import JournalHandler  # type: ignore
except ImportError:
    _log.debug("systemd.journal not available")
    JournalHandler = None


def notify_started() -> None:
    """Notify the system service manager, if any, that this program is
       fully up and running."""
    _log.info("initialization complete")
    notify("READY=1", True)


class ListenAddr(NamedTuple):
    addr: Optional[str]
    port: int


ntop6 = functools.partial(socket.inet_ntop, socket.AF_INET6)
ntop4 = functools.partial(socket.inet_ntop, socket.AF_INET)
pton6 = functools.partial(socket.inet_pton, socket.AF_INET6)
pton4 = functools.partial(socket.inet_pton, socket.AF_INET)


def addr_and_port(arg: str) -> ListenAddr:
    """Parse an address:port combination.  Three forms are recognized:
       - a.b.c.d:port - a.b.c.d must be a valid ipv4 address
       - [abcd::efgh]:port - abcd::efgh must be a valid ipv6 address
       - :port or just port - port only, listen on the wildcard address
    """
    def port(arg: str) -> int:
        pn = int(arg)
        if not (0 < pn <= 65535):
            raise ValueError("{} is not a valid TCP port".format(pn))
        return pn

    if not arg:
        raise ValueError("'' is not a valid address:port")
    if arg[0] == '[':
        addr, _, pn = arg.partition(']')
        addr = addr[1:]
        pn = pn[1:]
        # throws if invalid
        return ListenAddr(ntop6(pton6(addr)), port(pn))

    else:
        addr, _, pn = arg.partition(':')
        if not pn:
            return ListenAddr(None, port(addr))
        elif not addr:
            return ListenAddr(None, port(pn))
        else:
            return ListenAddr(ntop4(pton4(addr)), port(pn))


class ListeningSocket(NamedTuple):
    socket: socket.SocketType
    family: int  # AF_INET, etc
    type: int    # SOCK_STREAM, etc
    proto: int   # IPPROTO_TCP, etc
    port: int    # port number (-1 if not meaningful)


def preopened_listening_sockets() -> List[ListeningSocket]:
    """If this program was supplied sockets to listen on by the system
       service manager, return a sequence of ListeningSocket objects
       for each such socket.  Ensure that each socket is close-on-exec
       and has had listen() called on it.
    """
    sockets = []
    fds = listen_fds(unset_environment=True)
    try:
        for fd in fds:
            sock = socket.socket(fileno=fd)
            sname = sock.getsockname()
            if (
                    isinstance(sname, tuple)
                    and len(sname) >= 2
                    and isinstance(sname[1], int)
            ):
                port = sname[1]
            else:
                port = -1

            if sock.getsockopt(socket.SOL_SOCKET, socket.SO_ACCEPTCONN) == 0:
                sock.listen()

            sockets.append(ListeningSocket(
                sock, sock.family, sock.type, sock.proto, port
            ))

        _log.debug("found %d preopened listening sockets", len(sockets))
        return sockets

    except Exception:
        for fd in fds:
            os.close(fd)
        raise


def open_listening_sockets(addrs: Sequence[ListenAddr]
                           ) -> List[ListeningSocket]:
    """For each address in `addrs`, open a listening TCP socket."""
    ai_families = which_families_are_routable()
    ai_type = socket.SOCK_STREAM
    ai_proto = socket.IPPROTO_TCP
    ai_flags = socket.AI_PASSIVE | socket.AI_ADDRCONFIG

    sockets = []
    for addr in addrs:
        for ai_family in ai_families:
            try:
                ais = socket.getaddrinfo(
                    addr.addr, addr.port,
                    ai_family, ai_type, ai_proto, ai_flags
                )
            except OSError as e:
                _log.warning("getaddrinfo(%s, %d, %d) failed",
                             addr.addr, addr.port, ai_family,
                             exc_info=e)
                continue

            for (fm, ty, pr, _, sockaddr) in ais:
                try:
                    op = "socket"
                    sock = socket.socket(family=fm, type=ty, proto=pr)
                    op = "bind"
                    sock.bind(sockaddr)
                    op = "listen"
                    sock.listen()

                    sockets.append(ListeningSocket(
                        sock, fm, ty, pr, addr.port
                    ))

                except OSError as e:
                    _log.warning("%s(%d, %d, %d, %r) failed",
                                 op, fm, ty, pr, sockaddr, exc_info=e)
                    continue

    _log.debug("opened %d listening sockets", len(sockets))
    return sockets


@functools.lru_cache(maxsize=None)
def get_log_levels() -> Dict[str, int]:
    """Extract the set of named log levels from the logging module."""
    levels = {}
    for k in dir(logging):
        if k != k.upper():
            continue
        v = getattr(logging, k)
        if not isinstance(v, int) or v <= 0:
            continue
        levels[k.lower()] = v
    return levels


def journal_or_stderr_handler() -> logging.Handler:
    """Internal: determine whether stderr or stdout is connected to the
       systemd journal; return a JournalHandler if it is, a plain
       StreamHandler wrapping stderr otherwise.
    """
    if JournalHandler is not None:
        try:
            jstream = os.environ["JOURNAL_STREAM"]
            dev_, _, ino_ = jstream.partition(":")
            dev = int(dev_)
            ino = int(ino_)

            st = os.fstat(sys.stderr.fileno())
            if st.st_dev == dev and st.st_ino == ino:
                return cast(logging.Handler, JournalHandler())

            st = os.fstat(sys.stdout.fileno())
            if st.st_dev == dev and st.st_ino == ino:
                return cast(logging.Handler, JournalHandler())

        except Exception:
            # either JOURNAL_STREAM isn't set or we can't parse it
            pass

    return logging.StreamHandler(sys.stderr)


def config_log(min_level: str,
               log_file: Optional[str]) -> None:
    """Configure the root logger to send logs to LOG_FILE and filter out
       all logs with lower priority than MIN_LEVEL.

       MIN_LEVEL should be one of the keys of the dict returned by
       get_log_levels().

       If LOG_FILE is None, logs are sent to stderr.  If we have the
       client library and stderr is connected to the systemd journal,
       we transparently switch to the journal protocol.
    """
    root_handler: logging.Handler
    if log_file is not None:
        root_handler = logging.handlers.WatchedFileHandler(log_file)
    else:
        root_handler = journal_or_stderr_handler()

    if JournalHandler is not None and isinstance(root_handler, JournalHandler):
        # When logging to the journal, we don't need to annotate the message.
        formatter = logging.Formatter()
    else:
        formatter = logging.Formatter(
            fmt="%(asctime)s %(name)s: %(levelname)s: %(message)s"
        )

    formatter.default_time_format = "%Y-%M-%d %H:%M:%S"
    formatter.default_msec_format = "%s.%03d"
    root_handler.setFormatter(formatter)

    root_logger = logging.getLogger()
    root_logger.setLevel(get_log_levels()[min_level])
    root_logger.addHandler(root_handler)

    logging.captureWarnings(True)
