#! /bin/sh

# Run this script as the keyword-server user to initialize and/or
# update the git checkout and the virtualenv.

if [ $# -ne 0 ]; then
    printf 'usage: ./scripts/update\n' >&2
    exit 1
fi

PATH=/usr/local/bin:/usr/bin:/bin
export PATH
LC_ALL=C.UTF-8
export LC_ALL
LANGUAGE=C.UTF-8
export LANGUAGE
unset ENV BASH_ENV MAIL MAILPATH

set -e

# Before doing anything else, make sure this script is up to date.
if [ -d .git ]; then
    current_hash="$(sha256sum "$0")"
    git pull -q --ff-only
    new_hash="$(sha256sum "$0")"
    if [ "$current_hash" != "$new_hash" ]; then
        echo "Restarting with updated driver script." >&2
        exec "$0"
    fi
fi

# Find a suitably recent version of Python and, if necessary, create a
# virtualenv that uses it.  We need SSLContext.sni_callback, which was
# added in Python 3.7.
is_at_least_python_37 () {
    if "$1" -c 'import sys; sys.exit(sys.version_info < (3,7,0))' \
            > /dev/null 2>&1
    then return 0
    else return 1
    fi
}
if [ -n "$VIRTUAL_ENV" ]; then
    if is_at_least_python_37 python3; then :; else
        printf '%s: %s/bin/python3 is too old (need at least 3.7)\n' \
               "$0" "$VIRTUAL_ENV" >&2
        exit 1
    fi
elif [ -f ./venv/bin/activate ]; then
    VIRTUAL_ENV_DISABLE_PROMPT=t
    . venv/bin/activate
    if is_at_least_python_37 python3; then :; else
        printf '%s: %s/bin/python3 is too old (need at least 3.7)\n' \
               "$0" "$VIRTUAL_ENV" >&2
        exit 1
    fi
else
    python_found=
    for candidate in python3 python3.8 python3.7; do
        if is_at_least_python_37 $candidate; then
            # Use --system-site-packages so that python-systemd is accessible
            # if it's installed on the system.
            $candidate -m venv --system-site-packages venv
            python_found=t
            break
        fi
    done
    if [ -z "$python_found" ]; then
        printf '%s: no usable Python found (need at least 3.7)\n' "$0" >&2
        exit 1
    fi
    VIRTUAL_ENV_DISABLE_PROMPT=t
    . venv/bin/activate
fi
pip install -q --upgrade pip setuptools wheel
pip install -q -r requirements.txt
